'use strict';
(factory => {
    Hooks.once('init', () => {
        window.beto = factory();
    });
})(() => {
    const moduleResolvers = {};
    const modulePromises = {};
    const onCanvasReadyCallbacks = [];
    const canvasReadyPromise = new Promise(resolve => {
        Hooks.on('canvasReady', canvas => {
            resolve(canvas);
            onCanvasReadyCallbacks.forEach(callback => callback(canvas));
        });
    });

    const onCanvasReady = callback => {
        onCanvasReadyCallbacks.push(callback);
        if (canvas !== undefined) {
            setTimeout(() => {
                callback(canvas);
            });
        }
    };

    const calculateDistance = (actorToken, placeable) => {
        const xMinA = actorToken._validPosition.x;
        const yMinA = actorToken._validPosition.y;
        const xMaxA = xMinA + actorToken.hitArea.width;
        const yMaxA = yMinA + actorToken.hitArea.height;

        const xMinB = placeable._validPosition.x;
        const yMinB = placeable._validPosition.y;
        const xMaxB = xMinB + placeable.hitArea.width;
        const yMaxB = yMinB + placeable.hitArea.height;

        const deltaBeneath = ((xMinB - xMaxA) / 20);
        const deltaLeft = ((xMinA - xMaxB) / 20);
        const deltaAbove = ((yMinB - yMaxA) / 20);
        const deltaRight = ((yMinA - yMaxB) / 20);

        return 5 + Math.max(deltaBeneath, deltaLeft, deltaAbove, deltaRight);
    };

    const getTokenForActor = actor => {
        return canvas.tokens.placeables.find(placeable => placeable.actor?.id === actor.id);
    };

    const getActivePlayerTokens = canvas => {
        if (canvas.tokens.controlled.length > 0) {
            return canvas.tokens.controlled;
        }
        return [canvas.tokens.placeables.find(placeable => placeable.actor?.id === game.user.character.id)];
    };

    const getOnlyInSet = set => {
        if (set.size !== 1) {
            throw new Error('The set does not have exactly 1 item');
        }
        let returnItem;
        set.forEach(item => returnItem = item);
        return returnItem;
    };

    const getModule = moduleName => {
        if (Object.hasOwnProperty.call(modulePromises, moduleName)) {
            return modulePromises[moduleName];
        } else {
            modulePromises[moduleName] = new Promise(resolve => {
                moduleResolvers[moduleName] = resolve;
            });
            return modulePromises[moduleName];
        }
    };

    const registerModule = (moduleName, module) => {
        if (Object.hasOwnProperty.call(modulePromises, moduleName) && moduleResolvers[moduleName] !== undefined) {
            moduleResolvers[moduleName](module);
            delete moduleResolvers[moduleName];
        } else {
            modulePromises[moduleName] = new Promise(resolve => {
                resolve(module);
            });
        }
        return module;
    };

    const asyncEvery = async (array, callback) => {
        for (let index = 0; index < array.length; index++) {
            const response = await callback(array[index], index, array);
            if (response === false) {
                return false;
            }
        }
        return true;
    };

    const asyncForEach = async (array, callback) => {
        for (let index = 0; index < array.length; index++) {
            await callback(array[index], index, array);
        }
    };

    const beto = {
        getCanvas: () => canvasReadyPromise,
        onCanvasReady,
        calculateDistance,
        getOnlyInSet,
        getTokenForActor,
        getActivePlayerTokens,
        findTokenById: (canvas, id) => canvas.tokens.placeables.find(placeable => placeable.id === id),
        getReach: actor => actor.getFlag('beto-library', 'reach') ?? 5,
        canInteract: (actorToken, placeable) => calculateDistance(actorToken, placeable) <= beto.getReach(actorToken.document.actor),
        getModule,
        registerModule,
        asyncEvery,
        asyncForEach,
    };

    beto.registerModule('beto', beto);
    Hooks.callAll('beto.library.loaded', beto);

    return beto;
});
